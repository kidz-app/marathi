package kidz.pratham.marathi.database;
/**
 * Created by Pratham on 2/25/2017.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kidz.pratham.marathi.data.Album;
import kidz.pratham.marathi.data.AlbumScore;

/**
 * Created by Pratham on 5/20/2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, "marathi.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE `player_details` ( " +
                " `player_name` VARCHAR(30) NULL , `avatar` INT NOT NULL);");
        /*database.execSQL("CREATE TABLE `game_details` ( " +
                " `game_name` VARCHAR(30) NOT NULL " +
                ");" +
                "\n");*/
        database.execSQL("CREATE TABLE `high_score` ( " +
                " `player_name` VARCHAR(30) NULL , `avatar` INT NOT NULL, `game_name` VARCHAR(30) NULL , `score` FLOAT NOT NULL " +
                " );");
        database.execSQL("INSERT INTO `player_details`(`player_name`, `avatar`) VALUES" +
                " ( 'Pratham', " + Color.BLACK + " );");
        /*database.execSQL("INSERT INTO `game_details`(`game_name`) VALUES" +
                " ( 'match_the_pair');");
        database.execSQL("INSERT INTO `game_details`(`game_name`) VALUES" +
                " ( 'smaller_greater');");
        database.execSQL("INSERT INTO `game_details`(`game_name`) VALUES" +
                " ( 'money');");
        database.execSQL("INSERT INTO `game_details`(`game_name`) VALUES" +
                " ( 'flipper');");
        database.execSQL("INSERT INTO `game_details`(`game_name`) VALUES" +
                " ( 'guess_animal');");
        database.execSQL("INSERT INTO `game_details`(`game_name`) VALUES" +
                " ( 'alphabet');");
        database.execSQL("INSERT INTO `game_details`(`game_name`) VALUES" +
                " ( 'number');");*/

        database.execSQL("create unique index uk on `player_details`( `player_name`);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS `player_details`");
        database.execSQL("DROP TABLE IF EXISTS `game_details`");
        database.execSQL("DROP TABLE IF EXISTS `high_score`");
        onCreate(database);
    }

    public void insertPlayer(String name, int avatar) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("player_name", name);
        // Add userName extracted from Object
        values.put("avatar", avatar);

        long r = database.insert("player_details", null, values);
        Log.d("player_details returned", r + "");
        database.close();
    }
    public void executeMyQuery(String query) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL(query);
        database.close();
    }

    public List<Album> getAllPlayers() {
        List<Album> usersList = new ArrayList<>();

        String selectQuery = "SELECT player_name, avatar FROM player_details;";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                //usersList.add(new Album(cursor.getString(0), cursor.getInt(1)));
                //Log.d("pm", cursor.getString(0) + " " + cursor.getInt(1));
                Album a = new Album(cursor.getString(0), cursor.getInt(1));
                usersList.add(a);
            } while (cursor.moveToNext());
        }
        cursor.close();
        database.close();
        return usersList;
    }
    public List<AlbumScore> getHighScores() {
        List<AlbumScore> usersList = new ArrayList<>();

        String selectQuery = "SELECT player_name, avatar, SUM(score) FROM high_score GROUP BY (player_name) " +
                "ORDER BY(SUM(score)) DESC;";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        int i = 1;
        if (cursor.moveToFirst()) {
            do {
                //usersList.add(new Album(cursor.getString(0), cursor.getInt(1)));
                //Log.d("pm", cursor.getString(0) + " " + cursor.getInt(1));
                AlbumScore a = new AlbumScore(cursor.getString(0), cursor.getInt(1), cursor.getFloat(2), i++);
                usersList.add(a);
            } while (cursor.moveToNext());
        }
        cursor.close();
        database.close();
        return usersList;
    }

    public HashMap<String, Float> getAllHighScores(String player_name, int avatar) {
        HashMap<String, Float> usersList = new HashMap<>();

        String selectQuery = "SELECT player_name, avatar, game_name, MAX(score) " +
                "FROM high_score " +
                "where player_name = '" + player_name+ "' AND avatar = '" + avatar+ "' " +
                "GROUP BY player_name, game_name ;";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        Cursor dummy = database.rawQuery("select * from high_score", null);
        int i = 1;
        if (cursor.moveToFirst()) {
            do {
                //usersList.add(new Album(cursor.getString(0), cursor.getInt(1)));
                //Log.d("pm", cursor.getString(0) + " " + cursor.getInt(1));
                usersList.put(cursor.getString(2), cursor.getFloat(3));
            } while (cursor.moveToNext());
        }
        cursor.close();
        database.close();
        return usersList;
    }

    public void deleteTable(String table_name) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(table_name, null, null);
    }

    public void insert_score(String player_name, int avatar, String game_name, float score) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("player_name", player_name);
        // Add userName extracted from Object
        values.put("game_name", game_name);
        values.put("score", score);
        values.put("avatar", avatar);

        long r = database.insert("high_score", null, values);
        Log.d("high_score returned", r + "");
        database.close();
    }
}
