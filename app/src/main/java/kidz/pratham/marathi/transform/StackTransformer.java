package kidz.pratham.marathi.transform;

import android.view.View;

/**
 * Created by Pratham on 5/21/2017.
 */

public class StackTransformer extends ABaseTransformer {
    @Override
    protected void onTransform(View view, float position) {
        view.setTranslationX(position < 0 ? 0f : -view.getWidth() * position);
    }
}
