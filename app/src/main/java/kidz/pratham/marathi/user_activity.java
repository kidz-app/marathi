package kidz.pratham.marathi;
/**
 * Created by Pratham on 2/25/2017.
 */
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;

import kidz.pratham.marathi.data.Album;
import kidz.pratham.marathi.data.ColorPicker;
import kidz.pratham.marathi.database.DBHelper;

public class user_activity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AlbumsAdapter adapter;
    private List<Album> albumList;
    SharedPreferences preferences;
    DBHelper dbHelper;
    private Context mContext;
    private int mPickedColor = Color.BLACK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_activity);
        preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);
        setuserdetails();
        mContext = this;
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        dbHelper = new DBHelper(this);
        albumList = new ArrayList<>();
        adapter = new AlbumsAdapter(this, albumList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 6);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(01), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        findViewById(R.id.adduser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_add_user);
                dialog.setTitle("Add User");
                dialog.show();
                dialog.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String name = ((EditText)dialog.findViewById(R.id.name)).getText().toString();
                        if (!(name.equals("") || name == null)) {
                            dbHelper.insertPlayer(name, mPickedColor);
                            prepareAlbums();
                        }
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.dp_main).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Get a GridView object from ColorPicker class
                        GridView gv = (GridView) ColorPicker.getColorPicker(user_activity.this);

                        // Initialize a new AlertDialog.Builder object
                        AlertDialog.Builder builder = new AlertDialog.Builder(user_activity.this);

                        // Set the alert dialog content to GridView (color picker)
                        builder.setView(gv);

                        // Initialize a new AlertDialog object
                        final AlertDialog dialog1 = builder.create();

                        // Show the color picker window
                        dialog1.show();

                        // Set the color picker dialog size
                        dialog1.getWindow().setLayout(
                                getScreenSize().x,
                                getScreenSize().y - getStatusBarHeight());

                        // Set an item click listener for GridView widget
                        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                // Get the pickedColor from AdapterView
                                mPickedColor = (int) parent.getItemAtPosition(position);
                                ((ImageView)dialog.findViewById(R.id.dp_main)).setColorFilter(mPickedColor);

                                // Set the layout background color as picked color

                                //rl.setBackgroundColor(mPickedColor);

                                // close the color picker
                                dialog1.dismiss();
                            }
                        });
                    }
                });
            }
        });
        prepareAlbums();
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(user_activity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.adding_user);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText(" प्लेयर जोडा वर क्लिक करुन नवीन प्लेयर जोडा.");
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

                // Custom method to get the screen width in pixels
    private Point getScreenSize(){
        WindowManager wm = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        //Display dimensions in pixels
        display.getSize(size);
        return size;
    }

    // Custom method to get status bar height in pixels
    public int getStatusBarHeight() {
        int height = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            height = getResources().getDimensionPixelSize(resourceId);
        }
        return height;
    }

    /**
     * Adding few albums for testing
     */
    private void prepareAlbums() {
        albumList.clear();
        //dbHelper.deleteTable("player_details");
        /*int[] covers = new int[]{
                R.color.colorAccent,
                R.color.colorPrimaryDark,
                R.color.colorPrimary};
        Random random = new Random();


        for (int i = 0 ; i< 1 ; i ++ ) {
            dbHelper.insertPlayer("user " + random.nextInt(), covers[random.nextInt(3)]);
        }*/
        albumList = new ArrayList<>(dbHelper.getAllPlayers());
        adapter = new AlbumsAdapter(this, albumList);
        recyclerView.setAdapter(adapter);
        //Log.d("prathm", albumList.get(10).getName());
        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    private class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.MyViewHolder> {
        private Context mContext;
        private List<Album> albumList;
        int lastPosition = -1;
        private final View.OnClickListener mOnClickListener = new MyOnClickListener();

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title;
            public ImageView thumbnail;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.username);
                thumbnail = (ImageView) view.findViewById(R.id.dp);
            }
        }


        public AlbumsAdapter(Context mContext, List<Album> albumList) {
            this.mContext = mContext;
            this.albumList = albumList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.user_card, parent, false);
            itemView.setOnClickListener(mOnClickListener);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            Album album = albumList.get(position);
            holder.title.setText(album.getName());

            holder.thumbnail.setColorFilter(album.getThumbnail());
        }

        @Override
        public int getItemCount() {
            return albumList.size();
        }

        private class MyOnClickListener implements View.OnClickListener {
            @Override
            public void onClick(View v) {
                int itemPosition = recyclerView.getChildLayoutPosition(v);
                Album item = albumList.get(itemPosition);
                ((ImageView) findViewById(R.id.dp_main)).setColorFilter(item.getThumbnail());
                ((TextView) findViewById(R.id.username)).setText(item.getName());

                SharedPreferences preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("current_user_name", item.getName());
                editor.putInt("current_user_thumbnail", item.getThumbnail());
                editor.apply();
                prepareAlbums();
            }
        }

    }
    private void setuserdetails() {
        String current_user_name = preferences.getString("current_user_name", "Pratham");
        int current_user_thumbnail = preferences.getInt("current_user_thumbnail", android.R.color.black);
        ((ImageView) findViewById(R.id.dp_main)).setColorFilter(current_user_thumbnail);
        ((TextView) findViewById(R.id.username)).setText(current_user_name);
    }
}
