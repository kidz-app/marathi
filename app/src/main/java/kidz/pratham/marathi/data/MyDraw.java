package kidz.pratham.marathi.data;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import kidz.pratham.marathi.R;

/**
 * Created by Pratham on 5/24/2017.
 */

public class MyDraw implements View.OnClickListener {
    Context context;
    String[] names;
    ViewPager mViewPager;
    ImageView imageView;
    TextView textView;
    Bitmap bitmap;
    Canvas canvas;
    Paint paint;
    float downx = 0, downy = 0, upx = 0, upy = 0;

    public MyDraw(Context context, String[] alphabets, ViewPager mViewPager) {
        this.context = context;
        names = alphabets;
        this.mViewPager = mViewPager;

    }
    @Override
    public void onClick(View v) {
        Dialog dialog = new Dialog(context, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_draw);
        textView = (TextView) dialog.findViewById(R.id.text);
        textView.setDrawingCacheEnabled(true);
        textView.buildDrawingCache();
        textView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        textView.layout(0, 0, textView.getMeasuredWidth(), textView.getMeasuredHeight());
        dialog.findViewById(R.id.layout).setBackground(new BitmapDrawable(context.getResources(),
                textView.getDrawingCache()));

        imageView = (ImageView) dialog.findViewById(R.id.ChoosenImageView);

        dialog.show();

        View view = dialog.findViewById(R.id.layout);
        //float dw = window.getWidth();
        //float dh = window.getHeight();

        dialog.getWindow().setLayout(500, 500);
        float dw = 500;
        float dh = 500;

        bitmap = Bitmap.createBitmap((int) dw, (int) dh,
                Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        paint = new Paint();
        paint.setColor(Color.GREEN);
        paint.setStrokeWidth(20);

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        imageView.setImageBitmap(bitmap);

        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        downx = event.getX();
                        downy = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        upx = event.getX();
                        upy = event.getY();
                        canvas.drawLine(downx, downy, upx, upy, paint);
                        imageView.invalidate();
                        downx = upx;
                        downy = upy;
                        break;
                    case MotionEvent.ACTION_UP:
                        upx = event.getX();
                        upy = event.getY();
                        canvas.drawLine(downx, downy, upx, upy, paint);
                        imageView.invalidate();
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }
}