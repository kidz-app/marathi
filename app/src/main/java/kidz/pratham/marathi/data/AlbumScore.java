package kidz.pratham.marathi.data;

/**
 * Created by Pratham on 2/22/2017.
 */

public class AlbumScore {
    private String name;
    private int thumbnail;
    private int rank;
    private float score;

    public AlbumScore() {
    }

    public AlbumScore(String name, int thumbnail, int score) {
        this.name = name;
        this.thumbnail = thumbnail;
        this.score = score;
    }
    public AlbumScore(String name, int thumbnail, float score, int rank) {
        this.name = name;
        this.thumbnail = thumbnail;
        this.score = score;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
