package kidz.pratham.marathi;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RatingBar;

import java.util.HashMap;

import kidz.pratham.marathi.database.DBHelper;
import kidz.pratham.marathi.game.Game1_1;
import kidz.pratham.marathi.game.Game2;
import kidz.pratham.marathi.game.Game3;
import kidz.pratham.marathi.game.Game7;
import kidz.pratham.marathi.game.Game_5;

public class MainActivity extends AppCompatActivity {
    RatingBar ratingBar;
    int rating[] = {R.id.rt_alphabet,R.id.rt_number,R.id.rt_birds,R.id.rt_flowers,
            R.id.rt_game1,R.id.rt_game2,R.id.rt_game3,R.id.rt_game4,
            R.id.rt_game5, R.id.rt_game6,R.id.rt_animals, R.id.rt_fruits};
    DBHelper dbHelper;
    SharedPreferences preferences;
    int[] txtview_id = {
            R.id.button_alphabet, R.id.button_number, R.id.button_game2, R.id.button_game1,
            R.id.button_game3, R.id.button_game4, R.id.button_game5, R.id.button_animals,
            R.id.button_birds, R.id.button_fruits,  R.id.button_game6, R.id.button_flowers
    };
    String game_names[] = {"alphabet", "number", "birds", "flipper",
            "money", "guess_animal", "smaller_greater", "game4",
            "match_the_pair",  "flowers", "animals", "fruits"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        dbHelper = new DBHelper(this);

        //adding animation to views
        final Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide);
        for (int i = 0; i < txtview_id.length; i++) {
            findViewById(txtview_id[i]).setAnimation(animation);
        }
        preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);
        setRating();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            setRating();
        }
        super.onWindowFocusChanged(hasFocus);
    }

    private void setRating() {
        HashMap<String, Float> high_scores = dbHelper.getAllHighScores(preferences.getString("current_user_name", "Pratham"),
                preferences.getInt("current_user_thumbnail", android.R.color.black));
        //initAnimation();
        for(int i=0;i<rating.length;i++){
            ratingBar= (RatingBar) findViewById(rating[i]);
            ratingBar.setIsIndicator(true);
            if (high_scores.containsKey(game_names[i]))
                ratingBar.setRating(Math.round(high_scores.get(game_names[i]) * 3));
            else
                ratingBar.setRating(0);
            float current = ratingBar.getRating();

            ObjectAnimator anim = ObjectAnimator.ofFloat(ratingBar, "rating", 0, current);
            anim.setDuration(2500);
            anim.start();
        }
    }
    public void onClickHandler(View view) {
        int id = view.getId();
           Intent intent = null;
            switch (id) {
                case R.id.button_alphabet:
                    intent = new Intent(this, Alphabets.class);
                    break;
                case R.id.button_number:
                    intent = new Intent(this, Numbers.class);
                    break;
                case R.id.button_game2:
                    intent = new Intent(this, Game2.class);
                    break;
                case R.id.button_game1:
                    intent = new Intent(this,Game1_1.class);
                    break;
                case R.id.button_game3:
                    intent = new Intent(this, Game3.class);
                    break;
                case R.id.button_game4:
                    intent = new Intent(this, Barakhadi.class);
                    break;
                case R.id.button_game5:
                    intent = new Intent(this, Game_5.class);
                    break;
                case R.id.button_animals:
                    intent = new Intent(this, Animals.class);
                    break;
                case R.id.button_birds:
                    intent = new Intent(this, Birds.class);
                    break;
                case R.id.button_fruits:
                    intent = new Intent(this, Fruits.class);
                    break;
                case R.id.button_game6:
                    intent = new Intent(this, Flowers.class);
                    break;
                case R.id.button_flowers:
                    intent = new Intent(this, Game7.class);
                    break;
            }
            startActivity(intent);
    }
}
