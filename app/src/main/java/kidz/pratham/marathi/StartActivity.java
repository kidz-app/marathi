package kidz.pratham.marathi;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.Locale;

public class StartActivity extends AppCompatActivity {
    public static String __PRRFS__ = "Myprefs_pratham";
     VideoView videoview;
    SharedPreferences preferences;
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        videoview.start();
        setuserdetails();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Locale locale = new Locale("mr");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        setContentView(R.layout.activity_start);
        preferences = getSharedPreferences(__PRRFS__, MODE_PRIVATE);
        setuserdetails();
        videoview = (VideoView) findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.lion);

        videoview.setVideoURI(uri);
        videoview.start();
        ImageView imageView = (ImageView)findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(StartActivity.this);
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                //finish();
            }
        });
        final ImageView leaderboard = (ImageView) findViewById(R.id.leaderboard);
        leaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LeaderBoard.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(StartActivity.this,
                        leaderboard, "leader");
                startActivity(intent, options.toBundle());
            }
        });

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.user);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), user_activity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(StartActivity.this,
                        linearLayout, "user");
                startActivity(intent, options.toBundle());
            }
        });
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Info.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(StartActivity.this,
                        v, "about_us");
                startActivity(intent, options.toBundle());
            }
        });
    }

    private void setuserdetails() {
        String current_user_name = preferences.getString("current_user_name", "Pratham");
        int current_user_thumbnail = preferences.getInt("current_user_thumbnail", android.R.color.black);
        ((ImageView) findViewById(R.id.dp_main)).setColorFilter(current_user_thumbnail);
        ((TextView) findViewById(R.id.username)).setText(current_user_name);
    }
}
