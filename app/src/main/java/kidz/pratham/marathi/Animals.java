package kidz.pratham.marathi;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.VideoView;

import kidz.pratham.marathi.database.DBHelper;

public class Animals extends AppCompatActivity {
    int animals[]={R.raw.lion,R.raw.zebra,R.raw.rabbit,R.raw.tiger,
            R.raw.donkey,R.raw.beer,R.raw.cat,
            R.raw.chittah,R.raw.cow, R.raw.crocodial,
            R.raw.elephant,R.raw.kolha,R.raw.pan_genda,
            R.raw.monkey,R.raw.jiraaf,R.raw.goat,R.raw.horse,R.raw.dog};
    String anames[] = {"सिंह","झेब्रा","ससा", "वाघ", "गाढव","अस्वल","मांजर","चित्ता","गाय","मगर","हत्ती",
            "कोल्हा","पाणघोडा","माकड","जिराफ","शेळी","घोडा","कुत्रा"};
    VideoView videoview;
    static int i=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_animals);
        videoview = (VideoView) findViewById(R.id.videoView3);

        setText(anames[i]);
        playVideo(animals[i]);

        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Animals.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.animals);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText("व्हिडिओ आणि नाव यावरून प्राणी ओळखा आणि गुण मिळवा.");                dialog.show();
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    public void playVideo(int url) {
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+url);
        //Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.lion);
        final VideoView videoview = (VideoView) findViewById(R.id.videoView3);
        videoview.setVideoURI(uri);
        videoview.start();
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
    }
    public void setText(String name){
        TextView textView = (TextView)findViewById(R.id.textView3);
        textView.setText(name);
    }
    public void onClickHandler(View view){
        int id = view.getId();
        switch(id){
            case R.id.forward:
                    i++;
                    if(i>=animals.length){
                        i=0;
                    }
                    playVideo(animals[i]);
                    setText(anames[i]);
                    break;
            case R.id.backward:

                    if(i > 0)
                        i--;
                    playVideo(animals[i]);
                    setText(anames[i]);
                    break;
        }

    }

    @Override
    public void onBackPressed() {
        DBHelper dbHelper = new DBHelper(this);
        SharedPreferences preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);

        int result = Math.round(((float)(i + 1) / (float) animals.length) * 100);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.result_dialog);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setIsIndicator(true);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setStepSize(1);

        ((TextView)dialog.findViewById(R.id.score)).setText("" + result+ "%");
        if (result >= 90) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(3);
        }
        else if (result >= 70) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(2);
        }
        else if (result >= 50) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(1);
        }
        else {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(0);
        }
        dialog.show();
        (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Animals.super.onBackPressed();
            }
        });
        dbHelper.insert_score(preferences.getString("current_user_name", "Pratham"),
                preferences.getInt("current_user_thumbnail", Color.BLACK)
                ,"animals", (((RatingBar)dialog.findViewById(R.id.rating_score)).getRating()) / 3);

        ////Toast.makeText(this, "" + ((RatingBar)dialog.findViewById(R.id.rating_score)).getRating() / 3, Toast.LENGTH_SHORT).show();
    }
}
