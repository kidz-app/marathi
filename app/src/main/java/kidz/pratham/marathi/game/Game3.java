package kidz.pratham.marathi.game;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.Random;

import kidz.pratham.marathi.R;
import kidz.pratham.marathi.StartActivity;
import kidz.pratham.marathi.database.DBHelper;

public class Game3 extends AppCompatActivity implements View.OnClickListener{
    private TextView textView_one, textView_two;
    private ImageView imageView, imageView1, imageView2;
    private boolean mokey_on_right = true;
    Animation animation;
    AnimatedVectorDrawable drawable_sad, drawable_happy;
    ImageView image;
    private SoundPool soundPool;
    private int soundID, soundID_wrong;
    boolean loaded = false;
    int correct_score = 0, total_score = 0;

    private void playSound(int sound) {
        // Getting the user sound settings
        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        // Is the sound loaded already?
        if (loaded) {
            soundPool.play(sound, volume, volume, 1, 0, 1f);
            Log.e("Test", "Played sound");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game3);

        textView_one = (TextView) findViewById(R.id.number1);
        textView_two = (TextView) findViewById(R.id.number2);
        imageView = (ImageView) findViewById(R.id.monkey);
        image = (ImageView) findViewById(R.id.check);
         drawable_sad = (AnimatedVectorDrawable) getResources().getDrawable(R.drawable.to_sad);
         drawable_happy = (AnimatedVectorDrawable) getResources().getDrawable(R.drawable.to_happy);

        // Set the hardware buttons to control the music
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        // Load the sound
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                loaded = true;
            }
        });
        soundID = soundPool.load(this, R.raw.cheers, 1);
        soundID_wrong = soundPool.load(this, R.raw.wrong, 1);

        imageView1 = (ImageView) findViewById(R.id.machine_one);
        imageView2 = (ImageView) findViewById(R.id.machine_two);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_monkey);
        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int fi = Integer.parseInt(textView_one.getText().toString());
                int si = Integer.parseInt(textView_two.getText().toString());
                if (fi > si && (!mokey_on_right)) {
                    Toast.makeText(getApplicationContext(), "बरोबर", Toast.LENGTH_SHORT).show();
                    imageView1.setImageDrawable(getResources().getDrawable(R.drawable.machine));
                    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.machine_up));
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void run() {
                            setData();
                        }
                    }, 2500);
                }
                else if (si > fi && mokey_on_right) {
                    Toast.makeText(getApplicationContext(), "बरोबर", Toast.LENGTH_SHORT).show();
                    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.machine));
                    imageView1.setImageDrawable(getResources().getDrawable(R.drawable.machine_up));
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void run() {
                            setData();
                        }
                    }, 2500);
                }
                else
                    Toast.makeText(getApplicationContext(), "चूक", Toast.LENGTH_SHORT).show();;
            }
        });*/
        setData();
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Game3.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.greater);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText("मोठया संख्याकडे बोट दाखवा आणि गुण मिळवा.");
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    private void setAnimation() {
        AnimatedVectorDrawable drawable1 = (AnimatedVectorDrawable) getResources().getDrawable(R.drawable.weight_anim);
        final AnimatedVectorDrawable drawable2 = (AnimatedVectorDrawable) getResources().getDrawable(R.drawable.weight_anim);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView1.setImageDrawable(drawable1);
            imageView2.setImageDrawable(drawable2);
            drawable1.start();
            final Handler handler = new Handler();
           // sleep(250);
            //drawable2.start();
            handler.postDelayed(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    drawable2.start();
                }
            }, 250);
        }
    }

    private void setData() {
        Random ran = new Random();
        int one = ran.nextInt(21);
        int two;
        textView_one.setText("" + one);
        boolean choice = ran.nextBoolean();
        if (choice) {
            two = Math.abs((one + ran.nextInt(21)) % 21);
        }
        else {
            two = Math.abs((one - ran.nextInt(21)) % 21);
        }
        if (one != two)
            textView_two.setText("" + two);
        else
            setData();
        image.setImageDrawable(getResources().getDrawable(R.drawable.ic_neutral));
        setAnimation();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.num1:
                if (mokey_on_right) {
                    mokey_on_right = false;
                    imageView.setScaleX(-1f);
                    imageView.startAnimation(animation);
                }
                break;
            case R.id.num2:
                if (!mokey_on_right) {
                    mokey_on_right = true;
                    imageView.setScaleX(1f);
                    imageView.startAnimation(animation);
                }
                break;
        }
        checkAns();
    }
    public void checkAns() {
        total_score++;
        int fi = Integer.parseInt(textView_one.getText().toString());
        int si = Integer.parseInt(textView_two.getText().toString());
        if (fi > si && (!mokey_on_right)) {
            //Toast.makeText(getApplicationContext(), "बरोबर", Toast.LENGTH_SHORT).show();
            imageView1.setImageDrawable(getResources().getDrawable(R.drawable.machine));
            imageView2.setImageDrawable(getResources().getDrawable(R.drawable.machine_up));
            playSound(soundID);
            final Handler handler = new Handler();
            image.setImageDrawable(drawable_happy);
            drawable_happy.start();
            handler.postDelayed(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    setData();
                }
            }, 2500);
            correct_score++;
        }
        else if (si > fi && mokey_on_right) {
            playSound(soundID);
            //Toast.makeText(getApplicationContext(), "बरोबर", Toast.LENGTH_SHORT).show();
            imageView2.setImageDrawable(getResources().getDrawable(R.drawable.machine));
            imageView1.setImageDrawable(getResources().getDrawable(R.drawable.machine_up));
            final Handler handler = new Handler();
            image.setImageDrawable(drawable_happy);
            drawable_happy.start();
            handler.postDelayed(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    setData();
                }
            }, 2500);
            correct_score++;
        }
        else {
            playSound(soundID_wrong);
            //Toast.makeText(getApplicationContext(), "चूक", Toast.LENGTH_SHORT).show();
            image.setImageDrawable(drawable_sad);
            drawable_sad.start();
        }
    }

    @Override
    public void onBackPressed() {
        DBHelper dbHelper = new DBHelper(this);
        SharedPreferences preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);

        int result = Math.round(((float)correct_score / (float) total_score) * 100);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.result_dialog);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setIsIndicator(true);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setStepSize(1);

        ((TextView)dialog.findViewById(R.id.score)).setText("" + result + "%");
        if (result >= 90) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(3);
        }
        else if (result >= 70) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(2);
        }
        else if (result >= 50) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(1);
        }
        else {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(0);
        }
        dialog.show();
        (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dbHelper.insert_score(preferences.getString("current_user_name", "Pratham"),
                preferences.getInt("current_user_thumbnail", Color.BLACK)
                ,"smaller_greater", (((RatingBar)dialog.findViewById(R.id.rating_score)).getRating()) / 3);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Game3.super.onBackPressed();
            }
        });
    }
}
