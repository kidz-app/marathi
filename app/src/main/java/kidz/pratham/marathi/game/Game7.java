package kidz.pratham.marathi.game;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import kidz.pratham.marathi.R;
import kidz.pratham.marathi.StartActivity;
import kidz.pratham.marathi.database.DBHelper;

public class Game7 extends AppCompatActivity implements View.OnClickListener {
    ImageView[] img;
    int[] img2;
    Random rm;
    //HashMap<?,?> hm;
    private AnimatorSet mSetRightOut;
    int count = 0;
    int shownImgNum = -1, num_of_try = 0;
    View shownView = null;

    int image[] = {R.drawable.bimage0,R.drawable.bimage1,R.drawable.bimage2,R.drawable.bimage3,R.drawable.bimage4
            ,R.drawable.bimage5,R.drawable.bimage6,R.drawable.bimage7,R.drawable.bimage8,
            R.drawable.bimage9,R.drawable.bimage10,
            R.drawable.bimage11,R.drawable.bimage12,R.drawable.bimage13,
            R.drawable.bimage14,R.drawable.bimage15,R.drawable.bimage16};
    private SoundPool soundPool;
    private int soundID, soundID_wrong;
    boolean loaded = false;

    private void playSound(int sound) {
        // Getting the user sound settings
        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        // Is the sound loaded already?
        if (loaded) {
            soundPool.play(sound, volume, volume, 1, 0, 1f);
            Log.e("Test", "Played sound");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game7);

        img = new ImageView[8];

        img[0] = (ImageView) findViewById(R.id.myimage1);
        img[1] = (ImageView) findViewById(R.id.myimage2);
        img[2] = (ImageView) findViewById(R.id.myimage3);
        img[3] = (ImageView) findViewById(R.id.myimage4);
        img[4] = (ImageView) findViewById(R.id.myimage5);
        img[5] = (ImageView) findViewById(R.id.myimage6);
        img[6] = (ImageView) findViewById(R.id.myimage7);
        img[7] = (ImageView) findViewById(R.id.myimage8);

        img[0].setOnClickListener(this);

        rm = new Random();
        setdata();
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Game7.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.match_pair);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText("चित्रे लक्षात ठेवा आणि एकसारख्या चित्रांची जोडी ओळखा आणि गुण मिळवा.");
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        // Load the sound
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                loaded = true;
            }
        });
        soundID = soundPool.load(this, R.raw.cheers, 1);
        soundID_wrong = soundPool.load(this, R.raw.wrong, 1);
        //loadAnimations();
        //mSetRightOut.setTarget(mCardFrontLayout);
        //mSetRightOut.start();
    }

    private void setdata() {
        HashSet<Integer> hs = new HashSet<>();
        while (hs.size() < 4) {
            hs.add(image[rm.nextInt(image.length)]);
        }
        Object[] imgs = hs.toArray();
        List<Integer> a = Arrays.asList(0,1,2,3,4,5,6,7);
        Collections.shuffle(a);
        int i =0;

        img2 = new int[8];
        for (Integer c:a) {
                //img[c.intValue()].setImageDrawable(getResources().getDrawable((Integer) imgs[i++ % 4]));
            img[i].setOnClickListener(this);
            img[i].setBackground(getResources().getDrawable(R.mipmap.ic_launcher));
                img2[c.intValue()] = (int) imgs[i++ % 4];
        }
    }

    @Override
    public void onClick(final View view) {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.card_flip_right_out);
        int nu = 0;

        switch (view.getId()) {
            case R.id.myimage1:
                nu = 0;
                break;
            case R.id.myimage2:
                nu = 1;
                break;
            case R.id.myimage3:
                nu = 02;
                break;
            case R.id.myimage4:
                nu = 03;
                break;
            case R.id.myimage5:
                nu = 04;
                break;
            case R.id.myimage6:
                nu = 05;
                break;
            case R.id.myimage7:
                nu = 06;
                break;
            case R.id.myimage8:
                nu = 07;
                break;
        }
        view.setBackground(getResources().getDrawable(img2[nu]));

        mSetRightOut.setTarget(view);
        mSetRightOut.start();
        final int finalNu = img2[nu];
        mSetRightOut.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setBackground(getResources().getDrawable(finalNu));
                //view.setBackground(getResources().getDrawable(R.mipmap.ic_launcher));

                if (shownImgNum == -1) {
                    shownImgNum = finalNu;
                    shownView = view;
                }
                else {
                    if ((shownImgNum == finalNu) && (!shownView.equals(view))) {
                        count++;
                        playSound(soundID);
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                        shownView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                        if (count == 4) {
                            count = 0;
                            showResult();
                        }
                    }
                    else {
                        view.setBackground(getResources().getDrawable(R.mipmap.ic_launcher));
                        shownView.setBackground(getResources().getDrawable(R.mipmap.ic_launcher));
                        playSound(soundID_wrong);
                    }
                    shownImgNum = -1;
                    shownView = null;

                    num_of_try++;
                }
                //mSetLeftIn.cancel();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void showResult() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.result_dialog);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setIsIndicator(true);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setStepSize(1);

        if (num_of_try < 7) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(3);
        }
        else if (num_of_try < 9) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(2);
        }
        else if (num_of_try < 11) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(1);
        }
        else {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(0);
        }
        float result = (int) Math.round(((RatingBar)dialog.findViewById(R.id.rating_score)).getRating() * 33.33);
        ((TextView)dialog.findViewById(R.id.score)).setText("" +
                Math.round(new Float(result).intValue())
                + "%");
        dialog.show();
        (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setdata();
            }
        });

        DBHelper dbHelper = new DBHelper(this);
        SharedPreferences preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);
        dbHelper.insert_score(preferences.getString("current_user_name", "Pratham"),
                preferences.getInt("current_user_thumbnail", Color.BLACK)
                ,"flipper", (((RatingBar)dialog.findViewById(R.id.rating_score)).getRating()) / 3);
    }
}