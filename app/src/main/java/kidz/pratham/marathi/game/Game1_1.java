package kidz.pratham.marathi.game;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.app.Dialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.Random;

import kidz.pratham.marathi.R;
import kidz.pratham.marathi.StartActivity;
import kidz.pratham.marathi.database.DBHelper;

public class Game1_1 extends AppCompatActivity {
    TextView count;
    int num, correct_score = 0, total_score = 0;
    private SoundPool soundPool;
    private int soundID, soundID_right, soundID_wrong;
    private Animation animation;
    ImageView piggy;
    boolean loaded = false;
    int toys[] = {
            R.drawable.aeroplane_toy, R.drawable.barbie_toy, R.drawable.bear_toy, R.drawable.car_toy,
            R.drawable.cube_toy, R.drawable.doll_toy, R.drawable.doll_toy, R.drawable.duck_toy,
            R.drawable.home_toy, R.drawable.horse_toy, R.drawable.train_toy
    };
    int toyNum = -1, toyPrice = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game1_1);

        findViewById(R.id.onerscoin).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.tenrsnote).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.hundredrsnote).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.tworscoin).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.fiftyrsnote).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.fiverscoin).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.twentyrsnote).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.fivehundred).setOnTouchListener(new MyTouchListener());
        //findViewById(R.id.piggy).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.piggylayout).setOnDragListener(new MyDragListener());
        piggy = (ImageView) findViewById(R.id.piggy);

        count = (TextView) findViewById(R.id.count);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.vibration);
        // Set the hardware buttons to control the music
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        // Load the sound
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                loaded = true;
            }
        });
        soundID = soundPool.load(this, R.raw.coin, 1);
        soundID_right = soundPool.load(this, R.raw.correct, 1);
        soundID_wrong = soundPool.load(this, R.raw.wrong, 1);
        setdata();
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Game1_1.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.money);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText("दिलेल्या किमतीनुसार पैसे जमा करा आणि गुण मिळवा.");
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }
    private void playSound(int sound) {
        // Getting the user sound settings
        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        // Is the sound loaded already?
        if (loaded) {
            soundPool.play(sound, volume, volume, 1, 0, 1f);
            Log.e("Test", "Played sound");
        }
    }

    private void setdata() {
        count.setText("0");

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.price_dialog);

        ImageView img = (ImageView) dialog.findViewById(R.id.toy);
        TextView txt = (TextView) dialog.findViewById(R.id.price);

        Random ran = new Random();
        toyNum = ran.nextInt(toys.length);
        img.setImageDrawable(getResources().getDrawable(toys[toyNum]));
        toyPrice = ((toyNum * ran.nextInt(1000)) + ran.nextInt(1000)) % 1000;
        txt.setText("" + toyPrice);
        ((TextView) findViewById(R.id.total_count)).setText(""+ toyPrice);
        dialog.show();
        dialog.findViewById(R.id.pricedialoglay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                        view);
                view.startDrag(data, shadowBuilder, view, 0);
                //view.setVisibility(View.INVISIBLE);
                switch (view.getId()) {
                    case R.id.hundredrsnote:
                        num = 100;
                        break;
                    case R.id.tenrsnote:
                        num = 10;
                        break;
                    case R.id.onerscoin:
                        num = 1;
                        break;
                    case R.id.tworscoin:
                        num = 2;
                        break;
                    case R.id.fiftyrsnote:
                        num = 50;
                        break;
                    case R.id.fiverscoin:
                        num = 5;
                        break;
                    case R.id.twentyrsnote:
                        num = 20;
                        break;
                    case R.id.fivehundred:
                        num = 500;
                        break;
                }
                return true;
            } else {
                return false;
            }
        }
    }

    class MyDragListener implements View.OnDragListener {
        Drawable enterShape = getResources().getDrawable(
                R.drawable.shape_droptarget);

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundDrawable(enterShape);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //v.setBackgroundDrawable(normalShape);
                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    /*View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    LinearLayout container = (LinearLayout) v;
                    container.addView(view);
                    view.setVisibility(View.VISIBLE);*/
                    int num1 = Integer.parseInt(count.getText().toString()) + num;
                    count.setText("" + num1);
                    playSound(soundID);
                    piggy.setAnimation(animation);

                    if (num1 == toyPrice) {
                        Toast.makeText(getApplicationContext(), "Winner", Toast.LENGTH_SHORT).show();
                        playSound(soundID_right);
                        correct_score++;
                        total_score++;
                        showWinningAnimation();
                    }
                    else if (num1 > toyPrice){
                        Toast.makeText(getApplicationContext(), "Loser", Toast.LENGTH_SHORT).show();
                        playSound(soundID_wrong);
                        total_score++;
                        setdata();
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setBackgroundColor(Color.TRANSPARENT);
                default:
                    break;
            }
            return true;
        }
    }

    private void showWinningAnimation() {
        final Dialog dialog = new Dialog(this, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.price_dialog);

        ImageView img = (ImageView) dialog.findViewById(R.id.toy);
        dialog.findViewById(R.id.text1).setVisibility(View.GONE);

        img.setBackground(getResources().getDrawable(toys[toyNum]));
        img.setImageDrawable(getResources().getDrawable(R.drawable.sold_out));
        dialog.show();
        dialog.findViewById(R.id.pricedialoglay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setdata();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DBHelper dbHelper = new DBHelper(this);
        SharedPreferences preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);

        int result = Math.round(((float)correct_score / (float) total_score) * 100);
       // dbHelper.insert_score(preferences.getString("current_user_name", "Pratham"),
         //       preferences.getInt("current_user_thumbnail", Color.BLACK)
           //     ,"money", ((float)result) / 100);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.result_dialog);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setIsIndicator(true);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setStepSize(1);

        ((TextView)dialog.findViewById(R.id.score)).setText("" + result+ "%");
        if (result >= 90) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(3);
        }
        else if (result >= 70) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(2);
        }
        else if (result >= 50) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(1);
        }
        else {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(0);
        }
        dialog.show();
        (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Game1_1.super.onBackPressed();
            }
        });
        dbHelper.insert_score(preferences.getString("current_user_name", "Pratham"),
                preferences.getInt("current_user_thumbnail", Color.BLACK)
                ,"money", (((RatingBar)dialog.findViewById(R.id.rating_score)).getRating()) / 3);
    }
}
