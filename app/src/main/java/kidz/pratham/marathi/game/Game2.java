package kidz.pratham.marathi.game;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import kidz.pratham.marathi.R;
import kidz.pratham.marathi.StartActivity;
import kidz.pratham.marathi.database.DBHelper;

public class Game2 extends AppCompatActivity {
    int i=0, correct_score = 0, total_score = 0;
    static int k = 0;
    String answer;
    Boolean verify;
    private SoundPool soundPool;
    private int soundID, soundID_wrong;
    boolean loaded = false;
    Set<Integer> set = new HashSet<>(4);
    Dialog dialog;
    int a[] = new int[4];
    String animals[]={"सिंह","वाघ","अस्वल","म्हैस","उंट","मांजर","चित्ता","गाय","हरीण","कुत्रा",
            "गाढव","हत्ती","कोल्हा","मासा","बेडूक","जिराफ","शेळी","बोकड","पणघोडा","घोडा","मुंगूस","माकड",
            "बैल","डुक्कर","ससा","उंदीर","गेंडा","मेंढी","लांडगा","झेब्रा"};
    private void playSound(int sound) {
        // Getting the user sound settings
        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        // Is the sound loaded already?
        if (loaded) {
            soundPool.play(sound, volume, volume, 1, 0, 1f);
            Log.e("Test", "Played sound");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);
        setData(k);
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Game2.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.animal);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText("प्राण्याचे योग्य नाव ओळखा आणि गुण मिळवा.");
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        // Load the sound
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                loaded = true;
            }
        });
        soundID = soundPool.load(this, R.raw.cheers, 1);
        soundID_wrong = soundPool.load(this, R.raw.wrong, 1);
    }

    private void setData(int animalsindex) {
        if(animalsindex > 30){
            animalsindex=0;
        }
        answer = animals[animalsindex];
        Random ran = new Random();
        set.clear();
        while (set.size() <= 4) {
            set.add(ran.nextInt(animals.length));
        }

        i = 0;

        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext() &&  i < 4) {
            a[i++] = iterator.next();
        }
        setImage("image"+animalsindex,animalsindex);
    }
    private void setValues(){

        Button but1 = (Button)findViewById(R.id.button1);
        Button but2 = (Button)findViewById(R.id.button2);
        Button but3 = (Button)findViewById(R.id.button3);
        Button but4 = (Button)findViewById(R.id.button4);
        List<Button> buttons = Arrays.asList(but1,but2,but3,but4);
        Collections.shuffle(buttons);
        i = 0;
        for (Button button:buttons){
            button.setText(animals[a[i]]);
            i++;
        }
    }

    private void setImage(String name,int index){
        verify = Arrays.asList(a).contains(index);
        if(!verify){
            a[2]=index;
        }
        ImageView imageView = (ImageView) findViewById(R.id.aimageView);
        Glide.with(this)
                .load(this.getResources().getIdentifier(name,"drawable",this.getApplicationInfo().packageName))
                .centerCrop()
                .crossFade()
                .override(200,150)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
       setValues();
    }

    private void creatDialog(String iname){
        dialog = new Dialog(this){
            @Override
            public boolean onTouchEvent(MotionEvent event) {
                this.dismiss();
                return true;
            }
        };
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.result_game);
        ImageView imageView = (ImageView) dialog.findViewById(R.id.resultView);

        imageView.setImageResource(this.getResources().getIdentifier(iname,"drawable",this.getApplicationInfo().packageName));
        dialog.show();
    }

    private void showWinningDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.congo_dialog);
        dialog.show();
        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Glide.with(dialog.getContext())
                .load(R.drawable.celebration)
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into((ImageView) dialog.findViewById(R.id.congo));

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setData((++k)%animals.length);
            }
        });
    }

    public void onClickHandler(View view) {
        total_score++;
                if(answer.equals(((Button) view).getText())) {
                   // name = "बरोबर";
                    correct_score++;
                    playSound(soundID);
                    showWinningDialog();
                }else{
                    String s = ((Button)view).getText().toString();
                    int a = Arrays.asList(animals).indexOf(s);
                    creatDialog("image"+a);
                    playSound(soundID_wrong);
                }
                //Toast.makeText(getApplicationContext(),name,Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onBackPressed() {
        DBHelper dbHelper = new DBHelper(this);
        SharedPreferences preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);

        int result = Math.round(((float)correct_score / (float) total_score) * 100);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.result_dialog);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setIsIndicator(true);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setStepSize(1);

        ((TextView)dialog.findViewById(R.id.score)).setText("" + result + "%");
        if (result >= 90) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(3);
        }
        else if (result >= 70) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(2);
        }
        else if (result >= 50) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(1);
        }
        else {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(0);
        }
        dialog.show();
        (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dbHelper.insert_score(preferences.getString("current_user_name", "Pratham"),
                preferences.getInt("current_user_thumbnail", Color.BLACK)
                ,"guess_animal", (((RatingBar)dialog.findViewById(R.id.rating_score)).getRating()) / 3);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Game2.super.onBackPressed();
            }
        });
    }
}
