package kidz.pratham.marathi.game;
/*
  Created by Pratham on 2/25/2017.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import kidz.pratham.marathi.R;
import kidz.pratham.marathi.StartActivity;
import kidz.pratham.marathi.database.DBHelper;

public class Game_5 extends AppCompatActivity {
    private SoundPool soundPool;
    private int soundID, soundID_correct;
    boolean loaded = false, isAnyOneClicked = false;
    ImageView but1, but2, but3, but4,but11, but22, but33, but44, firstClicked;
    TextView correct_count, total_count;
    Set firstDone, secondDone;
    Animator anim, anim2;
    int[] drawableIDS = {
            R.drawable.image0, R.drawable.hundred_rs_note, R.drawable.monkey, R.drawable.ic_pigbank,
            R.drawable.fimage0,R.drawable.fimage1,R.drawable.fimage2,R.drawable.fimage3,
            R.drawable.fimage4,R.drawable.fimage5,R.drawable.fimage6,R.drawable.fimage7,
            R.drawable.fimage8,R.drawable.fimage9,R.drawable.fimage10, R.drawable.fimage11,
            R.drawable.fimage12,R.drawable.fimage13,R.drawable.fimage14
    };
    String[] names = {"lion", "note", "monk", "pig", "0", "1", "2", "3", "4", "5", "6", "7", "8"
            , "9", "10", "11", "12", "13", "14"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game_5);

        correct_count = (TextView) findViewById(R.id.correct_count);
        total_count = (TextView) findViewById(R.id.total_count);

        // Set the hardware buttons to control the music
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        // Load the sound
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                loaded = true;
            }
        });
        soundID = soundPool.load(this, R.raw.wrong, 1);
        soundID_correct = soundPool.load(this, R.raw.correct, 1);

        but1 = (ImageView) findViewById(R.id.imageView1);
        but2 = (ImageView)findViewById(R.id.imageView2);
        but3 = (ImageView)findViewById(R.id.imageView3);
        but4 = (ImageView)findViewById(R.id.imageView4);
        but11 = (ImageView) findViewById(R.id.imageView11);
        but22 = (ImageView)findViewById(R.id.imageView21);
        but33 = (ImageView)findViewById(R.id.imageView31);
        but44 = (ImageView)findViewById(R.id.imageView41);

        firstDone = new HashSet(4);
        secondDone = new HashSet(4);
        setData();
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Game_5.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.jodi_julva);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText("एकसारख्या चित्रांची जोडी ओळखा आणि गुण मिळवा.");
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }
    List<ImageView> buttons, buttons2;
    private void setData() {
        firstDone.clear();
        secondDone.clear();
        buttons = Arrays.asList(but1,but2,but3,but4);
        buttons2 = Arrays.asList(but11,but22,but33,but44);
        Integer[] number = getRandom();
        Integer[] number2 = getRandom();
        int i = 0;
        Set image_nums = new HashSet();
        Random ran = new Random();
        while (image_nums.size() < 4) {
            image_nums.add(ran.nextInt(drawableIDS.length));
        }
        Object[] img_num = image_nums.toArray();
        //number = getRandom();
        //number2 = getRandom();
        while (i < 4){
            int num = (int) img_num[i];
            buttons.get(number[i]).setImageDrawable(getResources().getDrawable(drawableIDS[num]));
            buttons.get(number[i]).setTag(names[num]);
            buttons.get(number[i]).setVisibility(View.VISIBLE);
            buttons2.get(number2[i]).setImageDrawable(getResources().getDrawable(drawableIDS[num]));
            buttons2.get(number2[i]).setTag(names[num]);
            buttons2.get(number2[i]).setVisibility(View.VISIBLE);
            i++;
        }
    }

    private Integer[] getRandom() {
        List<Integer> a = Arrays.asList(0,1,2,3);
        Collections.shuffle(a);
        Integer b[] = new Integer[4];
        int i =0;
        for (Integer c:a) {
            b[i++] = c;
        }
        return b;
    }

    private void playSound(int sound) {
        // Getting the user sound settings
        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        // Is the sound loaded already?
        if (loaded) {
            soundPool.play(sound, volume, volume, 1, 0, 1f);
            Log.e("Test", "Played sound");
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (anim != null && anim2!= null)
            if ((anim.isRunning() || anim2.isRunning()))
                return;
        if (!isAnyOneClicked) {
            isAnyOneClicked = true;
            // Scaling
            Animation scale = new ScaleAnimation(1, 0.5f, 1, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
// 1 second duration
            scale.setDuration(1000);
            scale.setRepeatCount(Animation.INFINITE);
            scale.setRepeatMode(Animation.REVERSE);
            view.startAnimation(scale);
            firstClicked = (ImageView) view;
        }
        else {
            isAnyOneClicked = false;
            firstClicked.clearAnimation();
            final ImageView v = (ImageView) view;
            if (buttons.contains(firstClicked) && buttons2.contains(v)) {
                Object obj1 = firstClicked.getTag(), obj2 = v.getTag();
                if (obj1.equals(obj2)) {
                    if ((!firstDone.contains(firstClicked)) && (!secondDone.contains(v))) {
                        Toast.makeText(getApplicationContext(), "correct", Toast.LENGTH_SHORT).show();
                        firstDone.add(firstClicked);
                        secondDone.add(v);
                        playSound(soundID_correct);

                        removeAll(firstClicked, v);
                    }
                    else {
                        playSound(soundID);
                        total_count.setText("" + (Integer.parseInt(total_count.getText().toString()) + 1));
                    }
                }
                else {
                    playSound(soundID);
                    total_count.setText("" + (Integer.parseInt(total_count.getText().toString()) + 1));
                }
            }
            else if (buttons2.contains(firstClicked) && buttons.contains(v)) {
                Object obj1 = firstClicked.getTag(), obj2 = v.getTag();
                if (obj1.equals(obj2)) {
                    if ((!firstDone.contains(v)) && (!secondDone.contains(firstClicked))) {
                        Toast.makeText(getApplicationContext(), "correct", Toast.LENGTH_SHORT).show();
                        firstDone.add(v);
                        secondDone.add(firstClicked);
                        playSound(soundID_correct);

                        removeAll(firstClicked, v);
                    }
                    else {
                        playSound(soundID);
                        total_count.setText("" + (Integer.parseInt(total_count.getText().toString()) + 1));
                    }
                }
                else {
                    playSound(soundID);
                    total_count.setText("" + (Integer.parseInt(total_count.getText().toString()) + 1));
                }
            }
            else {
                playSound(soundID);
                total_count.setText("" + (Integer.parseInt(total_count.getText().toString()) + 1));
                Toast.makeText(getApplicationContext(), "wrong", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void removeAll(ImageView firstClicked, final ImageView v) {
            // get the center for the clipping circle
            int cx = this.firstClicked.getWidth() / 2;
            int cy = this.firstClicked.getHeight() / 2;

// get the initial radius for the clipping circle
            float initialRadius = (float) Math.hypot(cx, cy);

// create the animation (the final radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(this.firstClicked, cx, cy, initialRadius, 0);
            anim2 =
                    ViewAnimationUtils.createCircularReveal(v, cx, cy, initialRadius, 0);

// make the view invisible when the animation is done
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    Game_5.this.firstClicked.setVisibility(View.GONE);
                }
            });
            anim2.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    v.setVisibility(View.GONE);
                    if (firstDone.size() == 2) {
                        setData();
                    }
                    correct_count.setText(""+ (Integer.parseInt(correct_count.getText().toString()) + 1));
                    total_count.setText("" + (Integer.parseInt(total_count.getText().toString()) + 1));
                }
            });

// start the animation
            anim.setDuration(750);
            anim2.setDuration(750);
            anim.start();
            anim2.start();
    }
    @Override
    public void onBackPressed() {
        DBHelper dbHelper = new DBHelper(this);
        SharedPreferences preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);

        int correct_score = Integer.parseInt(correct_count.getText().toString());
        int total_score = Integer.parseInt(total_count.getText().toString());

        int result = Math.round(((float)correct_score / (float) total_score) * 100);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.result_dialog);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setIsIndicator(true);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setStepSize(1);

        ((TextView)dialog.findViewById(R.id.score)).setText("" + result + "%");
        if (result >= 90) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(3);
        }
        else if (result >= 70) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(2);
        }
        else if (result >= 50) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(1);
        }
        else {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(0);
        }
        dialog.show();
        (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dbHelper.insert_score(preferences.getString("current_user_name", "Pratham"),
                preferences.getInt("current_user_thumbnail", Color.BLACK)
                ,"match_the_pair", (((RatingBar)dialog.findViewById(R.id.rating_score)).getRating()) / 3);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Game_5.super.onBackPressed();
            }
        });
    }
}
