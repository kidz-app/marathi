package kidz.pratham.marathi;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.app.Dialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.VideoView;

public class Barakhadi extends AppCompatActivity {
    private String[] alphabets;
    private TextView textView;
    private int current_letter = 12;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_barakhadi);
        alphabets = getResources().getStringArray(R.array.alphabets);
        textView = (TextView) findViewById(R.id.letter);
        textView.setText(alphabets[current_letter]);
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Barakhadi.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.bharakhadi);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText("विविध काना, मात्रा क्लिक करुन बाराखडी शिका.");
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    public void onClick(View view) {
        if (view.getId() == R.id.next) {
            if (++current_letter == alphabets.length)
                current_letter = 12;
            textView.setText(alphabets[current_letter]);
        }
        else {
            textView.setText(alphabets[current_letter] + ((TextView) view).getText());
        }
    }
}
