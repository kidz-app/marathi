package kidz.pratham.marathi;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.animation.Animator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Info extends AppCompatActivity {
    ImageView imageView;
    ImageButton imageButton;
    LinearLayout revealView, layoutButtons;
    Animation alphaAnimation;
    float pixelDensity;
    boolean flag[];
    int num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_info);

        pixelDensity = getResources().getDisplayMetrics().density;
        flag = new boolean[3];
        flag[0] = flag[1] = flag[2] = true;
        alphaAnimation = AnimationUtils.loadAnimation(this, R.anim.alpha_anim);
    }
    public void launchTwitter(View view) {
        int id = view.getId();
        if (id == R.id.launchTwitterAnimation) {
            imageView = (ImageView) findViewById(R.id.imageView);
            imageButton = (ImageButton) findViewById(R.id.launchTwitterAnimation);
            revealView = (LinearLayout) findViewById(R.id.linearView);
            layoutButtons = (LinearLayout) findViewById(R.id.layoutButtons);
            num = 0;
        }
        else if (id == R.id.launchTwitterAnimation1) {
            imageView = (ImageView) findViewById(R.id.imageView1);
            imageButton = (ImageButton) findViewById(R.id.launchTwitterAnimation1);
            revealView = (LinearLayout) findViewById(R.id.linearView1);
            layoutButtons = (LinearLayout) findViewById(R.id.layoutButtons1);
            num = 1;
        }
        else {
            imageView = (ImageView) findViewById(R.id.imageView2);
            imageButton = (ImageButton) findViewById(R.id.launchTwitterAnimation2);
            revealView = (LinearLayout) findViewById(R.id.linearView2);
            layoutButtons = (LinearLayout) findViewById(R.id.layoutButtons2);
            num = 2;
        }
        /*
         MARGIN_RIGHT = 16;
         FAB_BUTTON_RADIUS = 28;
         */
        int x = imageView.getRight();
        int y = imageView.getBottom();
        x -= ((28 * pixelDensity) + (16 * pixelDensity));

        int hypotenuse = (int) Math.hypot(imageView.getWidth(), imageView.getHeight());

        if (flag[num]) {

            //imageButton.setBackgroundResource(android.R.drawable.ic_menu_close_clear_cancel);
            imageButton.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);

            FrameLayout.LayoutParams parameters = (FrameLayout.LayoutParams)
                    revealView.getLayoutParams();
            parameters.height = imageView.getHeight();
            revealView.setLayoutParams(parameters);

            Animator anim = ViewAnimationUtils.createCircularReveal(revealView, x, y, 0, hypotenuse);
            anim.setDuration(700);

            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    layoutButtons.setVisibility(View.VISIBLE);
                    layoutButtons.startAnimation(alphaAnimation);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            revealView.setVisibility(View.VISIBLE);
            anim.start();

            flag[num] = false;
        } else {

            //imageButton.setBackgroundResource(android.R.drawable.ic_menu_info_details);
            imageButton.setImageResource(android.R.drawable.ic_menu_info_details);

            Animator anim = ViewAnimationUtils.createCircularReveal(revealView, x, y, hypotenuse, 0);
            anim.setDuration(400);

            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    revealView.setVisibility(View.GONE);
                    layoutButtons.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            anim.start();
            flag[num] = true;
        }
    }
}
