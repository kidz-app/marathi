package kidz.pratham.marathi;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.Random;

import kidz.pratham.marathi.database.DBHelper;
import kidz.pratham.marathi.transform.TabletTransformer;

public class Birds extends AppCompatActivity {
    boolean loaded = false, playSound = false;
    public int[] soundID;
    private SoundPool soundPool;
    int curindex;
    public int[] sound={R.raw.bsound0,R.raw.bsound1,R.raw.bsound2,R.raw.bsound3,
            R.raw.bsound4,R.raw.bsound5,R.raw.bsound6,R.raw.bsound7,
            R.raw.bsound8,R.raw.bsound9,R.raw.bsound10,R.raw.bsound11,R.raw.bsound12,R.raw.bsound13,
            R.raw.bsound14,R.raw.bsound15,R.raw.bsound16};


    public void playSound(int sound) {
        // Getting the user sound settings
        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        // Is the sound loaded already?
        if (loaded && playSound) {
            soundPool.play(sound, volume, volume, 1, 0, 1f);
            Log.e("Test", "Played sound" + sound);
        }
    }
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_birds);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setPageTransformer(true, new TabletTransformer());
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                loaded = true;
            }

        });
        soundID = new int[sound.length];
        for (int i=0;i < sound.length;i++){
            soundID[i] = soundPool.load(this, sound[i], 1);
        }
        final ImageView imageView1 = (ImageView)findViewById(R.id.soundIcon);
        imageView1.setImageResource(R.drawable.smute);
        imageView1.setTag(R.drawable.smute);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int resourceID = (int)imageView1.getTag();
                switch (resourceID){
                    case R.drawable.smute:
                        imageView1.setImageResource(R.drawable.son);
                        imageView1.setTag(R.drawable.son);
                        playSound = true;
                        playSound(soundID[mViewPager.getCurrentItem()]);
                        curindex=mViewPager.getCurrentItem();
                        break;
                    case R.drawable.son:
                        imageView1.setImageResource(R.drawable.smute);
                        imageView1.setTag(R.drawable.smute);
                        playSound = false;
                        break;
                }
            }
        });
        findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Birds.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.video_dialog);
                // set this uri to one of alphabets
                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.birds);
                VideoView videoView = (VideoView)dialog.findViewById(R.id.videoView2);
                videoView.setVideoURI(uri);
                videoView.start();
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                ((TextView)dialog.findViewById(R.id.textView5)).setText("चित्र,नाव आणि आवाजाद्वारे पक्षी ओळखा");
                (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //Toast.makeText(getApplicationContext(), "hi" + position, Toast.LENGTH_SHORT).show();
                playSound(soundID[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }
    public static int i = 0;
    public static String[] birdNamesMarathi = {"शहामृग","कोंबडा","हंस","चिमणी","मोर","बगळा","करकोचा","गरूड","पोपट","बदक","घुबड",
            "कबूतर","कोकिळा","घार","गिधाड","सुतार","कावळा"};
    public static  String[] birdnamesEnglish = {"Ostrich","Cock","Goose / swan","Sparrow","Peacock","Crane","Stork","Eagle","Parrot","Duck","Owl",
            "pigeon","cuckoo","kite","vulture","woodpecker","Crow"};
    static int image[] = {R.drawable.bimage0,R.drawable.bimage1,R.drawable.bimage2,R.drawable.bimage3,R.drawable.bimage4
            ,R.drawable.bimage5,R.drawable.bimage6,R.drawable.bimage7,R.drawable.bimage8,R.drawable.bimage9,R.drawable.bimage10,
            R.drawable.bimage11,R.drawable.bimage12,R.drawable.bimage13,R.drawable.bimage14,R.drawable.bimage15,R.drawable.bimage16};
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        public static final String ARG_SECTION_NUMBER = "section_number";
      //  public static String[] bimg;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_birds, container, false);
            //bimg = getActivity().getResources().getStringArray(R.array.bird_img);

            final ImageView imageView = (ImageView) rootView.findViewById(R.id.bimageView);
            final TextView bnameMarathi = (TextView) rootView.findViewById(R.id.bnMarathi);
            final TextView bnameEnglish = (TextView) rootView.findViewById(R.id.bnEnglish);
            int[] bird=getActivity().getResources().getIntArray(R.array.bird);
            imageView.setImageResource(image[(getArguments().getInt(ARG_SECTION_NUMBER))-1]);
            bnameMarathi.setText(birdNamesMarathi[(getArguments().getInt(ARG_SECTION_NUMBER))-1]);
            bnameEnglish.setText(birdnamesEnglish[(getArguments().getInt(ARG_SECTION_NUMBER))-1]);

            Random random = new Random();
            rootView.findViewById(R.id.layout).setBackgroundColor(Color.rgb(150 + random.nextInt(100),
                    150 + random.nextInt(100),150 + random.nextInt(100)));

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return birdNamesMarathi.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
    @Override
    public void onBackPressed() {
        DBHelper dbHelper = new DBHelper(this);
        SharedPreferences preferences = getSharedPreferences(StartActivity.__PRRFS__, MODE_PRIVATE);

        int result = Math.round(((float)(mViewPager.getCurrentItem() + 1) / (float) birdNamesMarathi.length) * 100);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.result_dialog);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setIsIndicator(true);
        ((RatingBar)dialog.findViewById(R.id.rating_score)).setStepSize(1);

        ((TextView)dialog.findViewById(R.id.score)).setText("" + result+ "%");
        if (result >= 90) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(3);
        }
        else if (result >= 70) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(2);
        }
        else if (result >= 50) {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(1);
        }
        else {
            ((RatingBar)dialog.findViewById(R.id.rating_score)).setRating(0);
        }
        dialog.show();
        (dialog.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Birds.super.onBackPressed();
            }
        });
        dbHelper.insert_score(preferences.getString("current_user_name", "Pratham"),
                preferences.getInt("current_user_thumbnail", Color.BLACK)
                ,"birds", (((RatingBar)dialog.findViewById(R.id.rating_score)).getRating()) / 3);

        //Toast.makeText(this, "" + ((RatingBar)dialog.findViewById(R.id.rating_score)).getRating() / 3, Toast.LENGTH_SHORT).show();
    }
}
