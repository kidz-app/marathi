package kidz.pratham.marathi;
/**
 * Created by Pratham on 2/25/2017.
 */

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kidz.pratham.marathi.data.AlbumScore;
import kidz.pratham.marathi.database.DBHelper;

public class LeaderBoard extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AlbumsAdapter adapter;
    private List<AlbumScore> albumList;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //setContentView(R.layout.activity_user_activity);
        setContentView(R.layout.activity_leader_board);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        dbHelper = new DBHelper(this);
        albumList = new ArrayList<>();
        adapter = new AlbumsAdapter(this, albumList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 6);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(01), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareAlbums();
    }

    private void prepareAlbums() {
        albumList.clear();
        //dbHelper.deleteTable("player_details");
        /*int[] covers = new int[]{
                R.color.colorAccent,
                R.color.colorPrimaryDark,
                R.color.colorPrimary};
        Random random = new Random();


        for (int i = 0 ; i< 1 ; i ++ ) {
            dbHelper.insertPlayer("user " + random.nextInt(), covers[random.nextInt(3)]);
        }*/
        albumList = new ArrayList<>(dbHelper.getHighScores());
        adapter = new AlbumsAdapter(this, albumList);
        recyclerView.setAdapter(adapter);
        //Log.d("prathm", albumList.get(10).getName());
        adapter.notifyDataSetChanged();
    }
    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.MyViewHolder> {
        private Context mContext;
        private List<AlbumScore> albumList;
        int lastPosition = -1;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, score, rank;
            public ImageView thumbnail;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.username);
                score = (TextView) view.findViewById(R.id.userscore);
                rank = (TextView) view.findViewById(R.id.rank);
                thumbnail = (ImageView) view.findViewById(R.id.dp);
            }
        }


        public AlbumsAdapter(Context mContext, List<AlbumScore> albumList) {
            this.mContext = mContext;
            this.albumList = albumList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.score_card, parent, false);
            return new AlbumsAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            AlbumScore album = albumList.get(position);
            holder.title.setText(album.getName());
            holder.score.setText(album.getScore() + "");
            holder.rank.setText(album.getRank() + "");
            holder.thumbnail.setColorFilter(album.getThumbnail());
        }

        @Override
        public int getItemCount() {
            return albumList.size();
        }


    }
}
